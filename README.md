## MDC Helyek Android alkalmazás

A Mondriaan DC-nél megpályázott Android fejlesztői állás kiválasztási folyamata során kapott assignment megoldása.

### Branch-ek

Két megoldást készítettem el, amelyek a *master* és a *product_flavors* brancheken érhetőek el.

#### master

A *master* branchen egy hagyományos felépítésű Android projekt található. Itt a feladatkiírásban szereplő feature-ök közül egyetlen dolog nem került implementálásra, mégpedig az, hogy két különböző product flavor-t lehessen buildelni. 

Az egyetlen apk, ami itt buildelődik a szokásos lokalizációval (values, values-hu, strings.xml), illetve __programozottan oldja meg az angol és magyar locale-ek támogatását__ és a nyelvenként különböző asset-ek használatát.

#### product_flavors

A *product_flavors* branch-en két flavor került kialakításra a "market" dimension mentén:

* hungarian
* english

A projekt könyvtárstruktúrája is tükrözi a fenti flavor-öket.

A _build.gradle_ idevágó részlete a következő:

    flavorDimensions "market"
    
    productFlavors {
        hungarian {
            dimension "market"
            resValue "string", "app_name", "Mdc helyek"
        }

        english {
            dimension "market"
            resValue "string", "app_name", "Mdc Pois"
        }
    }

A két build type és a két product flavor így összesen négy _build variant_ elkészítését teszi lehetővé, melyek kiválaszthatóak az Android Studio _Build variants_ ablakában is:

* englishDebug
* englishRelease
* hungarianDebug
* hungarianRelease

### Parancssori fordítás és futtatás

A gradle wrapper tool segítségével parancssorból is készíthetünk APK-t, illetve installálhatjuk az alkalmazást a teszt eszközeinkre.

Az összes elérhető gradle task kilistáztatható a projekt gyökérkönyvtárából ezzel a paranccsal:

    ./gradlew/tasks

#### APK generálása 

Az *assemble* kezdetű gradle taskok használhatók arra, hogy egy adott flavor-höz (vagy akár egy lépésben az összeshez) létrehozzuk az APK fájlokat. Például:

    ./gradlew assembleEnglish

Az .apk kiterjesztésű fájlok itt jönnek létre: `<project_dir>/app/build/output/apk/<flavor_name>`

#### Telepítés eszközre vagy emulátorra

Az alkalmazás telepítése egy a számítógéphez csatlakoztatott Android eszközre vagy emulátorra az *install* kezdetű gradle parancsok valamelyikével történhet. Például:

    ./gradlew installHungarianDebug

### Hasznos linkek

[Build variánsok konfigurálása](https://developer.android.com/studio/build/build-variants?utm_source=android-studio#product-flavors)

[Build taskok parancssori végrehajtása](https://developer.android.com/studio/build/building-cmdline)

[Android Product Flavors](https://iammert.medium.com/android-product-flavors-1ef276b2bbc1)






