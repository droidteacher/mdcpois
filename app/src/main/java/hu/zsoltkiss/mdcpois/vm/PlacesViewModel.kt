package hu.zsoltkiss.mdcpois.vm

import androidx.lifecycle.ViewModel
import hu.zsoltkiss.mdcpois.datasource.RemoteDataSource
import hu.zsoltkiss.mdcpois.model.MDCPlace
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import okhttp3.Credentials

class PlacesViewModel: ViewModel() {

    private val disposables = CompositeDisposable()
    private val datasource = RemoteDataSource()
    private val credentials: String
        get() = Credentials.basic(AUTH_USER, AUTH_PW)

    private val places = BehaviorSubject.create<List<MDCPlace>>()

    fun getPlaces(): BehaviorSubject<List<MDCPlace>> {
        return places
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    fun fetchPlaces() {
        val disp = datasource.mdcPlacesObservable(credentials)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    places.onNext(it)
                },
                {
                    it.printStackTrace()
                }
            )

        disposables.add(disp)
    }

    companion object {
        private const val TAG = "__VM"
        private const val AUTH_USER = "candidate"
        private const val AUTH_PW = "Ahs6aiX7"
    }
}