package hu.zsoltkiss.mdcpois.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hu.zsoltkiss.mdcpois.R
import hu.zsoltkiss.mdcpois.model.ItemType
import hu.zsoltkiss.mdcpois.model.MDCHeader
import hu.zsoltkiss.mdcpois.model.MDCListItem
import hu.zsoltkiss.mdcpois.model.MDCPlace

class POIListAdapter(
    val items: MutableList<MDCListItem>,
    private val placeClickHandler: (place: MDCPlace) -> Unit,
    private val cityClickHandler: (city: String) -> Unit): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemType = ItemType.values()[viewType]
        val rootView = LayoutInflater.from(parent.context).inflate(itemType.layoutResource, parent, false)
        return when (itemType) {
            ItemType.HEADER -> HeaderViewHolder(rootView)
            else -> PlaceViewHolder(rootView)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val currentItem = items[position]

        when (currentItem.itemType) {
            ItemType.HEADER -> {
                (holder as HeaderViewHolder).cityTextView.text = (currentItem as MDCHeader).city

                holder.itemView.setOnClickListener {
                    cityClickHandler(currentItem.city)
                }
            }
            else -> {
                (holder as PlaceViewHolder).nameTextView.text = (currentItem as MDCPlace).name
                holder.addressTextView.text = currentItem.address
                holder.descriptionTextView.text = currentItem.description

                holder.itemView.setOnClickListener {
                    placeClickHandler(currentItem)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].itemType.ordinal
    }

    val spanSize: GridLayoutManager.SpanSizeLookup
        get() {
            return object: GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when(items[position].itemType) {
                        ItemType.HEADER -> 2
                        else -> 1
                    }
                }

            }
        }
}

class PlaceViewHolder(v: View): RecyclerView.ViewHolder(v) {
    val nameTextView: TextView = itemView.findViewById(R.id.tvLocationName)
    val addressTextView: TextView = itemView.findViewById(R.id.tvAddress)
    val descriptionTextView: TextView = itemView.findViewById(R.id.tvDescription)
}

class HeaderViewHolder(v: View): RecyclerView.ViewHolder(v) {
    val cityTextView: TextView = itemView.findViewById(R.id.tvCity)
}