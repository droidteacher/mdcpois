package hu.zsoltkiss.mdcpois.datasource

import hu.zsoltkiss.mdcpois.model.MDCPlace
import hu.zsoltkiss.mdcpois.network.RetrofitClient
import io.reactivex.Observable

class RemoteDataSource {

    fun mdcPlacesObservable(credentials: String): Observable<List<MDCPlace>> {
        return RetrofitClient.mdcApi.places(credentials)
    }

}