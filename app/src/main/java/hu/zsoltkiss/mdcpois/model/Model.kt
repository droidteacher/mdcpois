package hu.zsoltkiss.mdcpois.model

import com.google.android.gms.maps.model.LatLng
import hu.zsoltkiss.mdcpois.R
import java.io.Serializable

interface MDCListItem {
    val itemType: ItemType
}

data class MDCPlace(
    val name: String,
    val city: String,
    val address: String,
    val description: String,
    val coordinates: Coordinates
): MDCListItem, Serializable {
    override val itemType: ItemType
        get() = ItemType.PLACE
}

data class Coordinates(
    val latitude: Float,
    val longitude: Float
): Serializable {
    val latLng: LatLng
        get() = LatLng(latitude.toDouble(), longitude.toDouble())
}

data class MDCHeader(val city: String): MDCListItem {
    override val itemType: ItemType
        get() = ItemType.HEADER
}

enum class ItemType(val layoutResource: Int) {
    HEADER(R.layout.section_header_row),
    PLACE(R.layout.mdc_location_item)
}