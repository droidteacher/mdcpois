package hu.zsoltkiss.mdcpois.network

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.util.Log
import io.reactivex.subjects.PublishSubject

class NetworkChangeCallback(private val networkSubject: PublishSubject<Boolean>): ConnectivityManager.NetworkCallback() {

    override fun onCapabilitiesChanged(network: Network, networkCapabilities: NetworkCapabilities) {
        super.onCapabilitiesChanged(network, networkCapabilities)
        val hasInternetConnection = networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        val hasValidated = networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)

        if (hasInternetConnection && hasValidated) {
            // network is only guaranteed here for sure
            networkSubject.onNext(true)
        } else {
            networkSubject.onNext(false)
        }
    }

    override fun onAvailable(network: Network) {
        super.onAvailable(network)
        Log.d(TAG, "onAvailable: $network")
    }

    override fun onLost(network: Network) {
        super.onLost(network)
        Log.d(TAG, "onLost: $network")
        networkSubject.onNext(false)
    }

    companion object {
        private const val TAG = "__NET(callback)"
    }

}