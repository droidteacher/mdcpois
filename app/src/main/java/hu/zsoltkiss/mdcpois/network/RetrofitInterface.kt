package hu.zsoltkiss.mdcpois.network

import hu.zsoltkiss.mdcpois.model.MDCPlace
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header

interface RetrofitInterface {

    @GET("places")
    fun places(@Header("Authorization") basicAuthHeader: String): Observable<List<MDCPlace>>

}