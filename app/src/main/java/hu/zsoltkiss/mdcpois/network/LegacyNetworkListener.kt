package hu.zsoltkiss.mdcpois.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import io.reactivex.subjects.PublishSubject

class LegacyNetworkListener(private val connectivityManager: ConnectivityManager, private val connectionSubject: PublishSubject<Boolean>) : BroadcastReceiver() {

    override fun onReceive(ctx: Context?, intent: Intent?) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.N) {
            connectivityManager.activeNetworkInfo?.let { networkInfo: NetworkInfo ->
                when (networkInfo.state) {
                    NetworkInfo.State.CONNECTED -> connectionSubject.onNext(true)
                    else -> connectionSubject.onNext(false)
                }
            }
        }
    }
}