package hu.zsoltkiss.mdcpois.ui

import androidx.fragment.app.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import hu.zsoltkiss.mdcpois.R
import hu.zsoltkiss.mdcpois.model.MDCPlace
import java.io.Serializable

class POILocationFragment : Fragment() {

    private val callback = OnMapReadyCallback { googleMap ->

        googleMap.uiSettings.isZoomControlsEnabled = true

        val pois = arguments?.getSerializable(ARGS_POIS) as? List<MDCPlace>

        if (pois?.size == 1) {

            val mdcPoi = LatLng(pois[0].coordinates.latitude.toDouble(), pois[0].coordinates.longitude.toDouble())

            val marker = MarkerOptions().position(mdcPoi).title(pois[0].address).also {
                it.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            }

            googleMap.addMarker(marker)
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(mdcPoi))
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mdcPoi, 12.0f))

        } else {
            val builder = LatLngBounds.Builder()
            pois?.forEach {
                val mdcPoi = LatLng(it.coordinates.latitude.toDouble(), it.coordinates.longitude.toDouble())

                val marker = MarkerOptions().position(mdcPoi).title(it.name).also { markerOptions ->
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                }
                googleMap.addMarker(marker)

                builder.include(mdcPoi)
            }

            val bounds = builder.build()
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_poi_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }


    companion object {
        fun newInstance(pois: List<MDCPlace>): POILocationFragment {
            return POILocationFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARGS_POIS, pois as Serializable)
                }
            }
        }

        const val ARGS_POIS = "pois"
        private const val TAG = "__Frag(Map)"
    }
}