package hu.zsoltkiss.mdcpois.ui

import android.content.IntentFilter
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.ConnectivityManager.CONNECTIVITY_ACTION
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.LocaleList
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import hu.zsoltkiss.mdcpois.R
import hu.zsoltkiss.mdcpois.model.MDCPlace
import hu.zsoltkiss.mdcpois.network.LegacyNetworkListener
import hu.zsoltkiss.mdcpois.network.NetworkChangeCallback
import hu.zsoltkiss.mdcpois.vm.PlacesViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import java.util.*

interface ItemClickHandler {
    fun placeSelected(place: MDCPlace)
    fun citySelected(city: String)
}

class MainActivity : AppCompatActivity(), ItemClickHandler {

    private val disposables = CompositeDisposable()

    private var networkChangeCallback: NetworkChangeCallback? = null
    private var legacyNetworkListener: LegacyNetworkListener? = null
    private val networkSubject = PublishSubject.create<Boolean>()

    private lateinit var placesViewModel: PlacesViewModel

    private var isBackNavigationEnabled = false

    private val appBarLogoResourceId: Int
        get() {
            val deviceLanguage = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                LocaleList.getDefault().get(0).language
            } else {
                Locale.getDefault().language
            }

            return when (deviceLanguage == "hu") {
                true -> R.drawable.ic_action_bar_logo_hu
                else -> R.drawable.ic_action_bar_logo_en
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        placesViewModel = ViewModelProvider(this).get(PlacesViewModel::class.java)

        val disp1 = networkSubject.subscribe(
            { isOnline ->
                Log.d(TAG, "network changed >> isOnline: $isOnline")

                if (!isBackNavigationEnabled) {
                    // we are on list fragment
                    if (!isOnline) {
                        displayWaiting()
                    } else {
                        displayList()
                    }
                } else {
                    // we are on map fragment
                    if (!isOnline) {
                        Toast.makeText(this, R.string.error_no_network, Toast.LENGTH_LONG).show()
                    }
                }
            },
            {
                it.printStackTrace()
            }
        )
        disposables.add(disp1)

        registerNetworkListener()
    }

    override fun onStart() {
        super.onStart()
        if (checkConnectionAtStartup()) {
            displayList()
        } else {
            displayWaiting()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        networkChangeCallback?.let {
            (getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager).unregisterNetworkCallback(
                it
            )
        }

        legacyNetworkListener?.let {
            unregisterReceiver(it)
        }

        disposables.dispose()
    }

    override fun onSupportNavigateUp(): Boolean {
        if (isBackNavigationEnabled) {
            displayList()
        }

        return true
    }

    override fun placeSelected(place: MDCPlace) {
        displayMap(listOf(place))
    }

    override fun citySelected(city: String) {
        val placesInCity = placesViewModel.getPlaces().value?.filter { somePlace ->
            somePlace.city == city
        }

        placesInCity?.let {
            displayMap(it)
        }
    }

    // https://proandroiddev.com/connectivity-network-internet-state-change-on-android-10-and-above-311fb761925
    private fun registerNetworkListener() {
        val manager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            networkChangeCallback = NetworkChangeCallback(networkSubject)
            manager.registerDefaultNetworkCallback(networkChangeCallback!!)
        } else {
            legacyNetworkListener = LegacyNetworkListener(manager, networkSubject)
            registerReceiver(legacyNetworkListener, IntentFilter(CONNECTIVITY_ACTION))
        }
    }

    private fun displayList() {
        supportFragmentManager.popBackStack(ROOT_FRAGMENT, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        val tran = supportFragmentManager.beginTransaction()
        tran.replace(R.id.fragmentHolder, POIListFragment.newInstance())
        tran.addToBackStack(ROOT_FRAGMENT)
        tran.commit()
        customizePrimaryActionBar()
    }

    private fun displayMap(places: List<MDCPlace>) {
        val newFragment = POILocationFragment.newInstance(places)
        val tran = supportFragmentManager.beginTransaction()
        tran.replace(R.id.fragmentHolder, newFragment)
        tran.commit()
        val title = when (places.size == 1) {
            true -> places[0].name
            else -> places[0].city
        }
        customizeSecondaryActionBar(title)
    }

    private fun displayWaiting() {
        val newFragment = WaitingFragment()
        val tran = supportFragmentManager.beginTransaction()
        tran.replace(R.id.fragmentHolder, newFragment)
        tran.commit()
        customizePrimaryActionBar()
    }

    private fun customizePrimaryActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setLogo(appBarLogoResourceId)
        supportActionBar?.setDisplayUseLogoEnabled(true)
        supportActionBar?.title = null
        supportActionBar?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(this, R.color.action_bar_primary_bg)))
        isBackNavigationEnabled = false
    }

    private fun customizeSecondaryActionBar(title: String) {
        supportActionBar?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(this, R.color.action_bar_secondary_bg)))
        supportActionBar?.setDisplayUseLogoEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(false)
        supportActionBar?.title = title
        isBackNavigationEnabled = true
    }

    private fun checkConnectionAtStartup(): Boolean {
        val manager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        return manager.getNetworkCapabilities(manager.activeNetwork)?.hasCapability(
            NetworkCapabilities.NET_CAPABILITY_INTERNET) ?: false
    }

    companion object {
        private const val TAG = "__MAIN"
        private const val ROOT_FRAGMENT = "list_fragment"
    }
}