package hu.zsoltkiss.mdcpois.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hu.zsoltkiss.mdcpois.R
import hu.zsoltkiss.mdcpois.adapter.POIListAdapter
import hu.zsoltkiss.mdcpois.model.MDCHeader
import hu.zsoltkiss.mdcpois.model.MDCListItem
import hu.zsoltkiss.mdcpois.model.MDCPlace
import hu.zsoltkiss.mdcpois.vm.PlacesViewModel
import io.reactivex.disposables.CompositeDisposable

class POIListFragment : Fragment() {

    private val disposables = CompositeDisposable()
    private lateinit var placesViewModel: PlacesViewModel
    private lateinit var recyclerView: RecyclerView
    private var poiListAdapter = POIListAdapter(mutableListOf(), {
        (activity as ItemClickHandler).placeSelected(it)
    }) {
        (activity as ItemClickHandler).citySelected(it)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_poi_list, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        placesViewModel = ViewModelProvider(requireActivity()).get(PlacesViewModel::class.java)
        Log.d(TAG, "view model: $placesViewModel")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.placesRecyclerView)
        recyclerView.layoutManager = GridLayoutManager(requireActivity(), 2).apply { spanSizeLookup = poiListAdapter.spanSize }
        recyclerView.adapter = poiListAdapter
    }

    override fun onStart() {
        super.onStart()
        val disp1 = placesViewModel.getPlaces().subscribe {
            updateItems(it)
        }
        disposables.add(disp1)

        placesViewModel.fetchPlaces()
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
    }

    private fun updateItems(newItems: List<MDCPlace>) {

        val cities = mutableSetOf<String>().apply {
            addAll(newItems.map {
                it.city
            })
        }.toList().sorted()

        val allItems = mutableListOf<MDCListItem>()

        cities.forEach { someCity ->
            allItems.add(MDCHeader(someCity))

            newItems.filter {
                it.city == someCity
            }.forEach { somePlace ->
                allItems.add(somePlace)
            }
        }

        poiListAdapter.items.clear()
        poiListAdapter.items.addAll(allItems)
        poiListAdapter.notifyDataSetChanged()
    }

    companion object {

        @JvmStatic
        fun newInstance() = POIListFragment()

        private const val TAG = "__Frag(List)"
    }


}